import GameDetails from "./Components/GameDetails";
import Home from "./Components/Home";
import { BrowserRouter, Route } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <Route exact path="/" component={Home} />
      <Route path="/games/:id">
        <GameDetails />
      </Route>
    </BrowserRouter>
  );
}

export default App;
