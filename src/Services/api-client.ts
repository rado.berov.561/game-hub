import axios, {CanceledError}  from 'axios';

export default axios.create({
    baseURL : "https://api.rawg.io/api",
    params: {
        key: '2b125a07bfc74e0985f4f1b2bd984477'
    }
})