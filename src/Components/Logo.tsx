import { Link, Image } from "@chakra-ui/react";
import logo from "../assets/logo.webp";

const Logo = () => {
  return (
    <Link boxSize="80px" href="/">
      <Image src={logo} marginTop={2} />
    </Link>
  );
};

export default Logo;
