import { Game } from "../Hooks/useGames";
import {
  Card,
  Image,
  CardBody,
  Heading,
  HStack,
  CardFooter,
  Box,
  useBoolean,
  Link,
} from "@chakra-ui/react";
import PlatformIconList from "./PlatformIconList";
import CriticScore from "./CriticScore";
import getCroppedImmageUrl from "../Services/image-url";

interface Props {
  game: Game;
}

const GameCard = ({ game }: Props) => {
  const [isHovering, setIsHovering] = useBoolean(false);

  return (
    <>
      <Card height="340px">
        <Image src={getCroppedImmageUrl(game.background_image)} />
        <CardBody>
          <HStack marginBottom={3} justifyContent="space-between">
            <PlatformIconList
              platforms={game.parent_platforms.map((p) => p.platform)}
            />
            <CriticScore score={game.metacritic} />
          </HStack>
          <Heading fontSize="2xl">{game.name}</Heading>
        </CardBody>
      </Card>
      {isHovering && (
        <Box>
          <Image src={game.background_image} />
        </Box>
      )}
    </>
  );
};

export default GameCard;
