import { useParams } from "react-router-dom";
import { Grid, GridItem, HStack } from "@chakra-ui/react";
import NavBar from "./NavBar";
import Logo from "./Logo";

const GameDetails = () => {
  return (
    <Grid
      templateAreas={`"nav nav"
                        "aside main"`}
      templateColumns={{
        base: "1fr",
        lg: "80px 1fr",
      }}
    >
      <GridItem pl="2" area={"nav"}>
        <HStack padding="10px">
          <Logo />
        </HStack>
      </GridItem>
      <GridItem pl="2" area={"main"}>
        MAIN
      </GridItem>
      <GridItem pl="2" area={"aside"}>
        ASIDE
      </GridItem>
    </Grid>
  );
};

export default GameDetails;
